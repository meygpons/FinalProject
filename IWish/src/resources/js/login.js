$(document).ready(function(){
	// Login and Sign up
	$('a[id ^="local-"]').click(function(){
		console.log($('.login-active'));
		$($('.login-active').attr('href')).hide();
		$('.login-active').removeClass('login-active');
		$(this).addClass('login-active');
		$($(this).attr('href')).show();
	});

	$('.modal-signup').modal({
	   	dismissible: true, // Modal can be dismissed by clicking outside of the modal
	   	opacity: .5, // Opacity of modal background
	   	inDuration: 300, // Transition in duration
	   	outDuration: 200, // Transition out duration
	   	startingTop: '4%', // Starting top style attribute
	   	endingTop: '10%', // Ending top style attribute
	   	ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
	       	console.log(modal, trigger);
	     	},
	   	complete: function() {} // Callback for Modal close
	   	}
	);
		$('.modal-recover-email').modal({
	   	dismissible: true, // Modal can be dismissed by clicking outside of the modal
	   	opacity: .5, // Opacity of modal background
	   	inDuration: 300, // Transition in duration
	   	outDuration: 200, // Transition out duration
	   	startingTop: '4%', // Starting top style attribute
	   	endingTop: '10%', // Ending top style attribute
	   	ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
	       	console.log(modal, trigger);
	     	},
	   	complete: function() {} // Callback for Modal close
	   	}
	);


	$('.datepicker').pickadate({
		selectMonths: true, 
    	selectYears: 100,
		min: new Date(1900,01,01),
		// Mayor de 12 años
		max: new Date(new Date().getFullYear()-12, new Date().getMonth()+1, new Date().getDate()),
		today: 'Today',
		clear: 'Clear',
		close: 'OK'
	});

    // makes sure both the password and password2 field are the same before
    // submitting the form
    $("#password2").on("keyup", function () {
        let password = $("#password1").val();
        if (this.value != password) {
            this.setCustomValidity("The passwords don't match");
        } else {
            this.setCustomValidity("");
        }
    });

    $("#password1").on("change", function () {
        $("#password2").trigger("keyup");
    });


    // Para que no se pueda hacer más de un click al submit
	$('form').submit(function() {
    	$('input[type=submit]').prop('disabled', true).delay(900);
	});
});
