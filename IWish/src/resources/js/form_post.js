$(document).ready(function() {

	//all post events go here.
	InitEvents(true);



});

function showSlides(n) {
  var i;
  var slides = document.getElementsByClassName("mySlides");
  var dots = document.getElementsByClassName("dot");
  if (n > slides.length) {slideIndex = 1}    
  if (n < 1) {slideIndex = slides.length}
  for (i = 0; i < slides.length; i++) {
      slides[i].style.display = "none";  
  }
  for (i = 0; i < dots.length; i++) {
      dots[i].className = dots[i].className.replace(" active", "");
  }
  slides[slideIndex-1].style.display = "block";  
  dots[slideIndex-1].className += " active";
}

function InitEvents(isInit) {

	//if is not the first call, detach events and attach again to all buttons
	if (!isInit) {

		$('.btn-new-post').off();
		$('.btn-reply').off();
		$('.btn-edit').off();
		$('.btn-post').off();
		$('.modal-delete').off();
		$('.modal-users-like').off();
		$('.modal').off();
		$('input[type=file]').off();
		$('form').off();
		$('.btn-edit-post').off();

	}

	$('.btn-new-post').click(function(){
		$(this).siblings('.form-new-post').toggle();
	});

	// mostrar/ocultar formulario reply en post
	$('.btn-post').click(function(){
		$(this).parent().siblings('.form-post').toggle();
	});
	$('.btn-reply').click(function(){
		$(this).siblings('.form-reply').toggle();
	});

	// mostrar/ocultar formulario editar post
	$('.btn-edit').click(function(){
		$(this).siblings('.post-content').toggle();
		$(this).siblings('.post-content-edit').toggle();
	});

	$('.btn-edit-post').click(function(){
		$(this).parent().siblings('.post-content').toggle();
		$(this).parent().siblings('.post-content-edit').toggle();
	});

	$('.modal').modal({
	   	dismissible: true, // Modal can be dismissed by clicking outside of the modal
	   	opacity: .5, // Opacity of modal background
	   	inDuration: 300, // Transition in duration
	   	outDuration: 200, // Transition out duration
	   	startingTop: '4%', // Starting top style attribute
	   	endingTop: '10%', // Ending top style attribute
	   	ready: function(modal, trigger) { // Callback for Modal open. Modal and trigger parameters available.
	       	console.log(modal, trigger);
	     	},
	   	complete: function() {} // Callback for Modal close
	   	}
	);
	
	// Para que no se pueda hacer más de un click al submit
	$('form').submit(function() {

		//new post
		if ($("#title-post", this).length == 1) {


			if ($("#title-post", this).val() == "" && $("#content-post", this).val() == "" && $("#file-post", this).val() + "" == "") {

				$("#title-post", this)[0].setCustomValidity("Fill at least one field.");

				$(".button-submit", this).click();

				return false;

			}

		} else {

			if ($("#content-post", this).val() == "" &&  $("#file-post", this).val() + "" == "") {

				$("#content-post", this)[0].setCustomValidity("Put something or upload something.");

				$(".button-submit", this).click();

				return false;

			}

		}		

    	$('input[type=submit]').prop('disabled', true);

    	setTimeout(function() { $('input[type=submit]').prop('disabled', false); }, 900);

		return true;

	});

	//reset validity
	$("input[type=text]").on("change", function() {

		this.setCustomValidity("");

	});

	$("input[type=file]").on("change", function() {

		if ($(this).siblings("#title-post")[0]) {

			$(this).siblings("#title-post")[0].setCustomValidity("");

		}

		$(this).siblings("#content-post").setCustomValidity("");

	});

	$("textarea").on("change", function() {

		if ($(this).siblings("#title-post")[0]) {

			$(this).siblings("#title-post")[0].setCustomValidity("");

		}

		this.setCustomValidity("");

	});

	$('input[type=file]').on("change", function(event) {

		var reader = new FileReader();
		var $this = $(this);
	    reader.onload = function (e) {
	    	console.log($this);
	        $this
	        	.siblings('[id=output]')
	            .attr('src', e.target.result)
	            .width(150)
	            .height(200);
	    };

	    reader.readAsDataURL(event.target.files[0]);

	});

	//control posts vacios
/*	$(".form-edit").submit(function () {

		if ($("#content-post", this).val() == "" &&  $("#file-post", this).val() + "" == "") {

			$("#content-post", this)[0].setCustomValidity("Put something or upload something.");

			$(".btn-submit", this).click();

			console.log("A")

			return false;

		}

		return true;

	});*/

}