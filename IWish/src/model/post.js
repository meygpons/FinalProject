const mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// instància de l'esquema
var postSchema = new Schema({
	id_post_parent: ObjectId,
	id_user: ObjectId,
	username: String,
	users_like: [{
		username: String,
		name: String,
		lastname: String,
		image: String
	}],
	users_interested:[{
		username: String,
		name: String,
		lastname: String,
		image: String,
		email: String
	}],
	isBought: Boolean,
	recieved: Boolean,
	images: [{}],
	level_post: Number,
	post_type: String,
	title: String,
	content: String,
	post_date: Date,
	edit_date: Date,
	geolocation: [Number, Number]
});

postSchema.add({ replies: [postSchema]});

// registrem la col·lecció Users amb un esquema concret i exportem
module.exports = mongoose.model('Post', postSchema);