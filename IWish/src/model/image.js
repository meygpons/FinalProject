const mongoose = require('mongoose');

// objecte esuqema

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// instància de l'esquema
var imageSchema = new Schema({
	upload_date: Date,
	path: String,
	value: String,
	hash: String,
	size: Number
});



// registrem la col·lecció Users amb un esquema concret i exportem
module.exports = mongoose.model('Image', imageSchema);
