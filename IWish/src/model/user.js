const mongoose = require('mongoose');
const bcrypt   = require('bcrypt-nodejs');
const Post		= require("./post.js");
const PostSchema= mongoose.model('Post').schema;

var Schema = mongoose.Schema;
var ObjectId = Schema.ObjectId;

// instància de l'esquema
var userSchema = new Schema({
	id_posts_like: [ObjectId],
	id_interests: [ObjectId],
	id_posts_bought: [ObjectId],
	id_friends: [ObjectId],
	id_posts: [ObjectId],
	username: String,
	local : {
        email    : String,
		password : String,
		verified :  {type: Boolean, default: false}
	},
	facebook : {
        id      : String,
        token   : String,
        email   : String,
		name 	: String
	},
	google : {
		id      : String,
        token   : String,
        email   : String,
		name 	: String

	},
	posts: [PostSchema],
	profile: PostSchema,
	name: String,
	lastname: String,
	email: String,
	birth_date: Date,
	gender: String,
	tutorial: {type: Boolean, default: true}
});

/*
 * Generador de Hash
 */
userSchema.methods.generateHash = function(password) {
    return bcrypt.hashSync(password, bcrypt.genSaltSync(8), null);
};

/*
 * Comprueba si el password es correcto
 */
userSchema.methods.validPassword = function(password) {
    return bcrypt.compareSync(password, this.local.password);
};

userSchema.methods.changePassword = function(password, callback){
	var hash = this.generateHash(password);
	this.local.password = hash;
	this.save(function(err){
		callback(err);
	});
}

/*
 * registrem la col·lecció Users amb un esquema concret i exportem
 */
module.exports = mongoose.model('User', userSchema);
