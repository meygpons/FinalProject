const PATH_USER = '../../../model/user.js';

const User = require(PATH_USER);

/**
 * Metodo que cambia los datos del usuario
 */
function updateSettings(params, callback) {

	var user = params.user;
    var newPassword = params.password;
    var confirmPassword = params.confirmPassword;
    var currentPassword = params.currentPassword

	user.name = params.newName;
	user.lastname = params.newLastname;
	user.username = params.newUsername;
	user.birth_date = params.newBirth;

	if ((newPassword || confirmPassword || currentPassword) && 
        !(newPassword && confirmPassword && currentPassword)) {
        // if any of the password change fields exists, but not all of
        // them exist, cancel the update
        return callback(new Error("Some password fields were left blank"));
    }
    
    // simple wrapper to user.save, just in case
    function saveUser(err) {
        if (err) { return callback(err); }
	    user.save(callback);
    }
    
    if (newPassword) {
        // if the new password field exists
        if (newPassword != confirmPassword) {
            // if the password and the confirmation password aren't
            // equal, return an error
            return callback(new Error("The password fields don't match"));
        } else if (!user.validPassword(currentPassword)) {
            // the currentPassword doesn't match the current password (hurr)
            return callback(new Error("Invalid current password"));
        }

        // if all goes well
		user.changePassword(params.password, saveUser);
    } else {
        saveUser(); 
    }

}

module.exports = {
    updateSettings: updateSettings
}
