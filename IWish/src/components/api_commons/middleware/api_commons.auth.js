const JWT_HEADER_REGEX = /^Bearer ([^ ]*)/;
const apiCommons = require("../controller/api_commons.controller.js");

function getJwtFromHeader(header) {
    var capture = JWT_HEADER_REGEX.exec(header);
    
    if (capture == null) { return false; }
    return capture[1];
}

module.exports = function (req, res, next) {
    var headers = req.headers;
    var authorizationHeader = headers["authorization"];
    var jwt = getJwtFromHeader(authorizationHeader);
   
    var params = {};
    params.token = jwt;

    apiCommons.verifyToken(params, function (err, dec) {
        if (err) {
            res.status(401);
            res.send();
        } else {
            req.jwt = dec;
            next();
        }
    });

}
