const profileController = require("../controller/profile.controller.js");
const User  = require("../../../model/user.js");
const Promise = require("promise");
const path  = require('path');
const PATH_ERROR = path.resolve("../views/error/error.pug");

module.exports = function(app) {

    app.get("/profile/:idUser", function(req,res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
	    	params.idUser = req.params.idUser;

			//esto es una promise
		 	profileController.getProfileUserById(params).then(function(user){

				params.user = user;

		    	profileController.getProfilePosts(params, function(err, posts) {

		    		if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
		    		
					res.render('../views/profile/profile.pug', {
						user : params.user,
						posts: posts,
						userSession: req.user

					});
			
				});

			});
				
    		
		} else {
			res.render('../views/login/login.pug');
		}
		
    });
    
    app.post("/profile/newProfilePic", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
	    	params.files = req.files;
			params.user = req.user;

			profileController.addProfilePic(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
					
				res.redirect(req.get('referer'));

			});
				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });

    app.get("/profile/addFriend/:idNewFriend", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idNewFriend = req.params.idNewFriend;

			profileController.addNewFriend(params, function(err) {

				if (err) return res.render(PATH_ERROR);
				res.redirect(req.get('referer'));

			});
				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });

	app.get("/profile/removeFriend/:idFriend", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idFriend = req.params.idFriend;

			profileController.removeFriend(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
				res.redirect(req.get('referer'));

			});				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });
    
    app.get("/profile/getMorePosts/:lastPostDate/:idUserProfile", function(req, res) {


			var params = {};
			params.lastPostDate = req.params.lastPostDate;
			params.idUserProfile = req.params.idUserProfile;
			params.limit = 6;
			profileController.getMoreProfilePosts(params, function(err, posts) {

				if (err) return res.json({ error: 101, errorMessage: "There was an error getting more posts."});

				res.json( { d: posts } );
				
			});

    });

}
