const profileController = require("../controller/profile.controller.js");
const User  = require("../../../model/user.js");
const Promise = require("promise");
const path  = require('path');
const PATH_ERROR = path.resolve("../views/error/error.pug");

module.exports = function(app) {

    
     function profile(req,res) {
    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;

	    	profileController.getProfilePosts(params, function(err, posts) {

	    		if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });

	    		profileController.getUserFriendList(params, function(err, friends) {

	    			if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });

					params.nSuggestions = 5;

	    			profileController.getSuggestedUsers(params, function(err, suggestions) {

	    				if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });

						res.render('../views/profile/profile.pug', {
							user : params.user, // get the user out of session and pass to template
							posts: posts,
							userSession: req.user,
							myFriendList: friends,
							suggestions: suggestions

						});

					});
	    			
	    		});
		
			});
    		
		} else {
			res.render('../views/login/login.pug');
		}
		
    }

    app.get("/", profile);
    app.get("/profile", profile);

    app.get("/profile/:idUser", function(req,res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
	    	params.idUser = req.params.idUser;
	    	if (params.idUser == req.user.id) {
	    		res.redirect("/profile");
	    	}
			//esto es una promise
		 	profileController.getProfileUserById(params).then(function(user){

				params.user = user;

				//friends del profileUser
				profileController.getUserFriendList(params, function(err, userFriends) {

		    		if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });
	    			
		    		params.userSession = req.user;
		    		params.useUserSession = true;
	    			//friends del userSession
					profileController.getUserFriendList(params, function(err, myFriends) {

			    		if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });

				    	profileController.getProfilePosts(params, function(err, posts) {

				    		if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });

				    		params.nSuggestions = 3;

				    		profileController.getSuggestedUsers(params, function(err, suggestions) {

				    			if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });
					    		
								res.render('../views/profile/profile.pug', {
									user : params.user,
									posts: posts,
									userSession: req.user,
									myFriendList: myFriends,
									userFriendList: userFriends,
									suggestions: suggestions
								});

							});

						});

				    });

	    		});

			}).catch(function(err) {
				return res.render(PATH_ERROR, { error: 101, errorMessage: err.errorMessage });
			});
	 					
    		
		} else {
			res.render('../views/login/login.pug');
		}
		
    });
    
    app.post("/profile/newProfilePic", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
	    	params.files = req.files;
			params.user = req.user;

			profileController.addProfilePic(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
					
				res.redirect(req.get('referer'));

			});
				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });

    app.get("/profile/addFriend/:idNewFriend", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
	    	params.host = req.headers.host;
			params.user = req.user;
			params.idNewFriend = req.params.idNewFriend;

			profileController.addNewFriend(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
				res.redirect(req.get('referer'));

			});				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });

	app.get("/profile/removeFriend/:idFriend", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idFriend = req.params.idFriend;

			profileController.removeFriend(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
				res.redirect(req.get('referer'));

			});				
    		
		} else {
			res.render('../views/login/login.pug');
		}

    });

    app.post("/profile/help", function(req, res){
		if (req.isAuthenticated()) {

			var params = {};
			params.user = req.user;
			params.tutorial = req.body.tutorial;

			profileController.disableTutorial(params, function(err){
				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
				res.redirect(req.get('referer'));
			});

    	} else {
			res.render('../views/login/login.pug');
		}
    });
    

}