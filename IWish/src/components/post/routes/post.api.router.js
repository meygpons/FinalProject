const postController = require("../controller/post.controller.js");


const User  = require("../../../model/user.js");

module.exports = function(app) {

	//post new post
	app.post("/posts", function(req, res) {

		var params = {};
		params.postLevel = 0;
		params.body = req.body;
		params.files = req.files;
		params.user = req.user;

        // MAYBEFIXME: comprobar credenciales

		//llamar a controller y pasar params
		postController.postPost(params, function(err, data) {

			if (err) return res.json({ error: 101, errorMessage: "Could not save post." });
			res.json({ error: 0, post: data });

		});

	});

	//post new reply
	app.post("/posts/:idPost/:postLevel/:idUserProfile", function(req, res) {

		var params = {};
		params.postLevel = req.params.postLevel;
		params.idPost = req.params.idPost;
		params.body = req.body;
		params.files = req.files;
		params.user = req.user;
		params.idUserProfile = req.params.idUserProfile;

        // MAYBEFIXME: comprobar credenciales
		//llamar a controller y pasar params
		postController.postPost(params, function(err, data) {
			if (err) return res.json({ error: 101, errorMessage: "Could not post reply"});
			res.json({ error: 0, post: data });
		});

	});

	//post new reply of reply
	app.post("/posts/:idPost/:idReply/:postLevel/:idUserProfile", function(req, res) {

		var params = {};
		params.postLevel = req.params.postLevel;
		params.idPost = req.params.idPost;
		params.idReply = req.params.idReply;
		params.body = req.body;
		params.files = req.files;
		params.user = req.user;
		params.idUserProfile = req.params.idUserProfile;

        // MAYBEFIXME: controlar usuario logueado

		//llamar a controller y pasar params
		postController.postPost(params, function(err, data) {
			if (err) return res.json({ error: 101, errorMessage: "Could not post reply."});
			res.json({ error: 0, post: data });
		});

	});

	//post edit post
	app.post("/posts/edit/:idPost/:idReply/:idReplyOfReply/:idUserProfile", function(req, res) {

		if(req.isAuthenticated()) {

			var params = {};
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;
			params.newTitle = req.body.Title;
			params.newDescription = req.body.Description;
			params.files = req.files;
			params.user = req.user;
			params.idUserProfile = req.params.idUserProfile;

			postController.getPostOwner(params).then(function(user) {

				if (user._id.equals(req.user._id)) {

					postController.updatePost(params, function(err, data) {
						if (err) return res.json({ error: 101, errorMessage: "Could not edit post" });
                        // FIXME: return post contents!!!
						res.json({ error: 0 });
					});

				}

			});

			
		} else {
			res.json({ error: 1, errorMessage: "You are not logged in"});
		}

	});

	//delete post
	app.post("/posts/delete/:idPost/:idReply/:idReplyOfReply", function(req, res) {

		if (req.isAuthenticated()) {

			var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;

			//comprovar que el post es del usuario logeado.
			postController.getPostOwner(params).then(function(user) {

				if (user._id.equals(req.user._id)) {

					postController.deletePost(params, function(err) {

						if (err) return res.json({ error: 101, errorMessage: "Could not delete post."});
						res.json({ error: 0 });

					});

				} else res.json({ error: 102, errorMessage: "You are not the owner of the post." });

			});

		} else {
			res.json({ error: 1, errorMessage: "You are not logged in"});
		}

	});

	app.post("/posts/interest/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.interestPost(params, function(err) {
					if (err) return res.json({ error: 101, errorMessage: "Could not mark as interested"});
                    // FIXME: return info about the chat room
					res.json({ error: 0 });
				});

			});
    		
		} else {
			res.json({ error: 1, errorMessage: "You are not logged in"});
		}

    });

	app.post("/posts/buy/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.buyPost(params, function(err) {
					if (err) return res.json({ error: 101, errorMessage: "Could not mark as bought."  });
					res.json({ error: 0 });
				});

			});
    		
		} else {
			res.json({ error: 1, errorMessage: "You are not logged in"});
		}

    });

	app.post("/posts/like/:idPost/:idUserProfile", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;
			params.idUserProfile = req.params.idUserProfile;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.likePost(params, function(err, likes) {
					if (err) return res.json( { error: 101, errorMessage: "Could not post a like" } );
                    likes.error = 0;
					res.json(likes);
				});

			});
    		
		} else {
			res.json({ error: 1, errorMessage: "You are not logged in"});
		}

    });



}
