const path  = require('path');

const postController = require("../controller/post.controller.js");

const User  = require("../../../model/user.js");

const PATH_LOGIN = path.resolve('../views/login/login.pug');
const PATH_POST_FORM = path.resolve('../views/post_form/post_form.pug');
const PATH_PROFILE = path.resolve("/profile");
const PATH_ERROR = path.resolve("../views/error/error.pug");

module.exports = function(app) {

	//post edit post
	app.post("/posts/edit/:idPost/:idReply/:idReplyOfReply/:idUserProfile", function(req, res) {

		if(req.isAuthenticated()) {

			var params = {};
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;
			params.newTitle = req.body.Title;
			params.newDescription = req.body.Description;
			params.files = req.files;
			params.user = req.user;
			params.idUserProfile = req.params.idUserProfile;

			postController.getPostOwner(params).then(function(user) {

				if (user._id.equals(req.user._id)) {

					postController.updatePost(params, function(err, data) {

						if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
						res.redirect(req.get('referer'));
					});

				} else {

					return res.render(PATH_ERROR, { errorMessage: "Illegal action." });

				}

			});

			
		} else {

			res.render(PATH_LOGIN);
			
		}

	});
				
	//post new post
	app.post("/posts", function(req, res) {

		if(req.isAuthenticated()) {
			var params = {};
			params.postLevel = 0;
			params.body = req.body;
			params.files = req.files;
			params.user = req.user;

			//llamar a controller y pasar params
			postController.postPost(params, function(err, data) {

				if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
				res.redirect(req.get('referer'));

			});

		} else {

			res.render(PATH_LOGIN);

		}

	});
	
	//post new reply
	app.post("/posts/:idPost/:postLevel/:idUserProfile", function(req, res) {

		if(req.isAuthenticated()) {
			
			var params = {};
			params.postLevel = req.params.postLevel;
			params.idPost = req.params.idPost;
			params.body = req.body;
			params.files = req.files;
			params.user = req.user;
			params.idUserProfile = req.params.idUserProfile;


			//llamar a controller y pasar params
			postController.getPostOwner(params).then(function(postOwner) {
				params.postOwner = postOwner;	

				//llamar a controller y pasar params
				postController.postPost(params, function(err, data) {

					if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
					res.redirect(req.get('referer'));

				});

			});

		} else {

			res.render(PATH_LOGIN);

		}

	});

	//post new reply of reply
	app.post("/posts/:idPost/:idReply/:postLevel/:idUserProfile", function(req, res) {

		if(req.isAuthenticated()) {

			var params = {};
			params.postLevel = req.params.postLevel;
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.body = req.body;
			params.files = req.files;
			params.user = req.user;
			params.idUserProfile = req.params.idUserProfile;
			
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;	

				//llamar a controller y pasar params
				postController.postPost(params, function(err, data) {

					if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
					res.redirect(req.get('referer'));

				});
			});

		} else {

			res.render(PATH_LOGIN);

		}

	});


	//delete post
	app.get("/posts/delete/:idPost/:idReply/:idReplyOfReply/:idUserProfile", function(req, res) {

		if (req.isAuthenticated()) {

			var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;
			params.idUserProfile = req.params.idUserProfile;

			//comprovar que el post es del usuario logeado.
			postController.getPostOwner(params).then(function(user) {

				if (user._id.equals(req.user._id)) {

					postController.deletePost(params, function(err) {

						if (err) return res.render(PATH_ERROR, { errorMessage: err.errorMessage });
						res.redirect(req.get('referer'));

					});

				} else return res.render(PATH_ERROR, { errorMessage: "Illegal action." });

			});

		} else {

			res.render(PATH_ERROR);

		}

	});

	app.get("/posts/interest/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.interestPost(params, function(err) {
					if (err) return res.render(PATH_ERROR, { errorMessage: "Illegal action." });
					res.redirect(req.get('referer'));
				});

			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

    app.get("/posts/uninterest/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.uninterestPost(params, function(err) {

					if (err) return res.render(PATH_ERROR, { errorMessage: "Illegal action." });
					res.redirect(req.get('referer'));

				});

			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

    // Boton comprado
	app.get("/posts/buy/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				params.postOwner = user;

				postController.buyPost(params, function(err) {

					if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: "Illegal action." });

					postController.sendMailBought(params, function(err){

						if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: "Illegal action." });

						res.redirect(req.get('referer'));

					});

				});

			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

	app.get("/posts/like/:idPost/:idReply/:idReplyOfReply/:idUserProfile", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;
			params.idUserProfile = req.params.idUserProfile;

			postController.likePost(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: "Illegal action." });
				res.redirect(req.get('referer'));

			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

    app.get("/posts/unlike/:idPost/:idReply/:idReplyOfReply/:idUserProfile", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;
			params.idReply = req.params.idReply;
			params.idReplyOfReply = req.params.idReplyOfReply;
			params.idUserProfile = req.params.idUserProfile;

			postController.unlikePost(params, function(err) {

				if (err) return res.render(PATH_ERROR, { errorMessage: "Illegal action." });
				res.redirect(req.get('referer'));
				
			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

    // Boton recibido
	app.get("/posts/recieve/:idPost", function(req, res) {

    	if (req.isAuthenticated()) {

	    	var params = {};
			params.user = req.user;
			params.idPost = req.params.idPost;

			//necesito el propietario del post
			postController.getPostOwner(params).then(function(user) {

				//comprovar q el post es del usuario logeado.
				if (user._id.equals(req.user._id)) {

					params.postOwner = user;

					postController.recievePost(params, function(err) {

						if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: "Illegal action." });

						postController.sendMailRecieved(params, function(err) {

							if (err) return res.render(PATH_ERROR, { error: 101, errorMessage: "Illegal action." });

							res.redirect(req.get('referer'));

						});

					});

				} else {

					return res.render(PATH_ERROR, { error: 101, errorMessage: "Illegal action." });

				}

			});
    		
		} else {

			res.render('../views/login/login.pug');

		}

    });

}