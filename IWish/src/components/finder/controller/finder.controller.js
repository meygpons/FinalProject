const PATH_USER 		= '../../../model/user.js';
const User       		= require(PATH_USER);


function regexEscape(str) {
    return str.replace(/[-\/\\^$*+?.()|[\]{}]/g, '\\$&')
}

/**
 * Metodo que busca usuarios por el nombre a partir de una regex
 */
function findUsersByPartialName(params, callback) {

	var regex = new RegExp(regexEscape(params.userName) + ".*", 'i');

	User.find({ name: regex }, function(err, users) {

		callback(err, users);

	});

}

module.exports = { findUsersByPartialName: findUsersByPartialName }