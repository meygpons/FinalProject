const PATH_USER = '../../../../model/user.js';

const FacebookStrategy  = require('passport-facebook').Strategy;

const User       		= require(PATH_USER);
const configAuth        = require('../../auth');
const Post  = require("../../../../model/post.js");
const Image = require("../../../../model/image.js");

const PROFILE_PICTURE_PROTOCOL = "http";

module.exports = function(passport) {


    // facebook will send back the token and profile
    passport.use(new FacebookStrategy({
            clientID: configAuth.facebookAuth.clientID,
            clientSecret: configAuth.facebookAuth.clientSecret,
            callbackURL: configAuth.facebookAuth.callbackURL,
            profileFields: ["email", "name", "birthday", "photos"],
            passReqToCallback : true // allows us to pass in the req from our route (lets us check if a user is logged in or not)
        },
        function(req, token, refreshToken, profile, done) {
            //async
            process.nextTick(function() {

                //check if user is already logged in
                if (!req.user) {
                    // find the user in the database based on their email
                    User.findOne({ '$or' : [{ 'facebook.email' :  profile.emails[0].value}, { 'google.email' : profile.emails[0].value} , {'local.email' : profile.emails[0].value }]}, function (err, user) {

                        if (err) return done(err);

                        // if the user is found, then log them in
                        if (user) {

                            // if there is a user id already but no token (user was linked at one point and then removed)
                            if (user.facebook.id && !user.facebook.token) {
                                user.facebook.token = token; // we will save the token that facebook provides to the user                    
                                user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                                user.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

                                user.save(function(err) {
                                    if (err)
                                        throw err;

                                    // if successful, return the new user
                                    return done(null, user);
                                });
                            } else if(user.facebook.id == profile.id) {
                                //regular login
                                return done(null, user);
                            } 
                            return done(null, false);
                        } else {
                            // if there is no user found with that facebook id, create them
                            var newUser            = new User();

                            // set all of the facebook information in our user model
                            newUser.facebook.id    = profile.id; // set the users facebook id                   
                            newUser.facebook.token = token; // we will save the token that facebook provides to the user                    
                            newUser.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName; // look at the passport user profile to see how names are returned
                            newUser.facebook.email = profile.emails[0].value; // facebook can return multiple emails so we'll take the first

                            //crear primer post q sera el perfil (nivel 0)
                            var new_post = new Post();
                            new_post.level_post = 0;
                            new_post.post_type = "profile";
                            new_post.post_date = new Date();
                            new_post.edit_date = new Date();
                            new_post.id_user = newUser._id;
                            if(profile.photos){
                                var new_image = new Image();
                                new_image.path = genFacebookPhotoUrl(profile.id, PROFILE_PICTURE_PROTOCOL, 150, 150);
                                new_image.value = genFacebookPhotoUrl(profile.id, PROFILE_PICTURE_PROTOCOL, 150, 150);
                                new_post.images.push(new_image);
                                //new_post.images = profile.photos;
                            } else {
                                var new_image = new Image();
                                new_image.path = "../user_images/foto_basica.jpg";
                                new_image.value = "../user_images/foto_basica.jpg";
                                new_post.images.push(new_image);
                            }


                            newUser.profile = new_post;
                            newUser.username = profile.name.givenName;
                            newUser.name = profile.name.givenName;
                            newUser.lastname = profile.name.familyName;
                            newUser.email = profile.emails[0].value;
                            //newUser.emails.push.apply(newUser.emails, profile.emails); //guardamos todos los emails q nos da facebook
                            newUser.birth_date = new Date(profile._json.birthday);
                            // save our user to the database
                            newUser.save(function(err) {
                                if (err)
                                    throw err;

                                // if successful, return the new user
                                return done(null, newUser);
                            });
                        }
                    });
                //user is logged in, link accounts
                } else {
                    // user already exists and is logged in, we have to link accounts
                    User.findOne({ 'facebook.email' :  profile.emails[0].value}, function (err, user) {
                        if (err) return done(err);
                        if (user) {
                            return done(null, false);
                        }
                        var user            = req.user; // pull the user out of the session

                        //if user was linked once, we already have that email
                        /*if (!user.facebook.id) {
                            user.emails.push.apply(user.emails, profile.emails);
                        }*/
                        user.facebook.id    = profile.id;
                        user.facebook.token = token;
                        user.facebook.name  = profile.name.givenName + ' ' + profile.name.familyName;
                        user.facebook.email = profile.emails[0].value;
                        user.save(function(err) {
                            if (err)
                                throw err;
                            return done(null, user);
                        });
                    });
                }
            });
        }
    ));

}

function genFacebookPhotoUrl(id, protocol, width, height){
    return protocol + "://graph.facebook.com/"+id+"/picture?width="+width+"&height="+height;
}