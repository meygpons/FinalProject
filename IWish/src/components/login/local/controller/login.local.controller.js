const PATH_USER = '../../../../model/user.js';
const PATH_POST = '../../../../model/post.js';
 
const LocalStrategy = require('passport-local').Strategy;

const User = require(PATH_USER);
const Post  = require(PATH_POST);
const Image = require("../../../../model/image.js");


const confirmEmail = require('./login.local.confirm.controller.js');


module.exports = function(passport) {

  /*
   * Login local
   */
  passport.use('local-login', new LocalStrategy({
      // by default, local strategy uses username and password, we will override with email
      usernameField : 'email',
      passwordField : 'password',
      passReqToCallback : true // allows us to pass back the entire request to the callback
    }, function(req, email, password, done) { // callback with email and password from our form
      // find a user whose email is the same as the forms email
      // we are checking to see if the user trying to login already exists
      User.findOne({ 'local.email' :  email }, function(err, user) {
        // if there are any errors, return the error before anything else
        if (err) {
          return done(err);
        }
        // if no user is found, return the message
        if (!user) {
          return done(null, false); // req.flash is the way to set flashdata using connect-flash
        }
        if (!user.local.verified){
          return done(null, false); // el usuario aun no está validado
        }
        // if the user is found but the password is wrong
        if (!user.validPassword(password)){
          return done(null, false); // create the loginMessage and save it to session as flashdata
        }

        // all is well, return successful user
        return done(null, user);
        });
    })
  );

  /*
   * Local Sign up
   */

  passport.use('local-signup', new LocalStrategy({
    // by default, local strategy uses username and password, we will override with email
      usernameField : 'email',
      passwordField : 'password',
      passReqToCallback : true
    }, function(req, email, password, done) {
          if (req.body.password2 != password) {
              done(null, false);
          }
        
          if(req.user) {
            User.findOne({ 'local.email' :  email}, function (err, user) {
              if (err) return done(err);
              if (user && user.local.verified) {
                  return done(null, false);
              }
              console.log("AAAAAAAAAAAAAAA");
              // si estamos logueados y ya tenemos una cuenta local en verified no se podrá linkear
              
              var user            = req.user;
              user.local.email    = email;
              user.local.password = user.generateHash(password);

              params = {
                user : req.user,
                email : email
              };
              // @FIXME AVISAR DE QUE SE HA ENVIADO UN CORREO DE CONFIRMACION
              confirmEmail.sendEmail(params, function(){return;});
              
              user.save(function(err) {
              if (err) throw err;
                return done(null, user);
              }); 
            });

        // Si no estamos logueados
        } else {

          // check if both password
          // check if that email is already in use
          User.findOne({ '$or' : [{'local.email' :  email}, {'google.email' : email}, {'facebook.email' : email}]}, function(err, user) {
          var params;

          // if there are any errors, return the error
          if (err) {
            return done(err);
          }
          // si ha encontrado un usuario con ese email
          if (user) {
            if (user.google.email == email) {
              //@FIXME AVISAR DE QUE EL EMAIL YA ESTÁ EN USO POR OTRA VIA LINKEAR DESDE AHÍ
              return done(null, false);
            }
            if (user.facebook.email == email) {
              //@FIXME AVISAR DE QUE EL EMAIL YA ESTÁ EN USO POR OTRA VIA LINKEAR DESDE AHÍ
              return done(null, false);
            }
            // mira si el usuario login local esta verificado. Si está verificado no podrá hacer sign up con este email
            if(user.local.verified){
              return done(null, false);
            } else {
              // si no esta verificado quiere decir que no ha llegado el correo de confirmación o no ha dado al link
              // por lo tanto volvemos a recoger los datos por si ha cambiado algo y lo modificamos
              user.name           = req.body.name;
              user.lastname       = req.body.lastname;
              user.username       = req.body.username;
              if (req.body.birth)
                user.birth_date     = new Date(req.body.birth);
              user.gender         = req.body.gender;

              user.local.email    = email;
              user.local.password = user.generateHash(password);

              params = {
                user : user,
                email : email
              };
              user.save(function (err) { 
                if (err) return done(err);
                confirmEmail.sendEmail(params, function(){return;});
              });
              return done(null, user);
            }
          }
          //no estamos logeados creamos cuenta nueva
          var newUser            = new User();
          //crear primer post q sera el perfil (nivel 0)
          var new_post = new Post();
          new_post.level_post = 0;
          new_post.post_type = "profile";
          new_post.post_date = new Date();
          new_post.edit_date = new Date();
          new_post.id_user = newUser._id;
          
          var new_image = new Image();
          new_image.path = "../user_images/foto_basica.jpg";
          new_image.value = "../user_images/foto_basica.jpg";
          new_image.upload_date = new Date();
          new_post.images.push(new_image);

          newUser.profile = new_post;
          // set the user's local credentials
          newUser.name           = req.body.name;
          newUser.lastname       = req.body.lastname;
          newUser.username       = req.body.username;
          if (req.body.birth) {
            newUser.birth_date     = new Date(req.body.birth);
          }
          newUser.gender         = req.body.gender
          newUser.email          = email;
          newUser.local.email    = email;
          newUser.local.password = newUser.generateHash(password); // use the generateHash function in our user model
          
          params = {
            user  : newUser,
            email : email,
            host  : req.get('host')
          };
          confirmEmail.sendEmail(params, function(){return;});
          // save the user
          newUser.save(function(err) {
            if (err) throw err;
              return done(null, newUser);
            });
          
        });
      }
    }));
  
}
