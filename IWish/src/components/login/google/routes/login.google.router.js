const path       = require('path');
const PATH_ERROR = path.resolve('../views/error/error.pug');

var message = {
    err         : "There has been some error, try again.",
    send        : "Success! Check your email to recover your password.",
    signup      : "Email is been sent at Please check inbox",
    singup_err  : "That email is already in use, in facebook or local.",
    login_err   : "Invalid email or password."
}

module.exports = function(app, passport) {
    /*
    * Login by google
    */
    app.get('/auth/google', passport.authenticate('google', { scope : ['profile', 'email'] }));

    // the callback after google has authenticated the user
    app.get('/auth/google/callback',
        passport.authenticate('google', {
                successRedirect : '/profile',
                failureRedirect : '/failure/google'
        }));

    app.get('/failure/google', function(req, res){
        if (!req.isAuthenticated()) {
            res.render('../views/login/login.pug', {message: message.singup_err});
        } 
    })


    // connect = authorize facebook (link accounts)
    app.get('/connect/google', passport.authorize('google', { scope : ['profile', 'email'] }));

    // the callback after google has authorized the user
    app.get('/connect/google/callback',
        passport.authorize('google', {
            successRedirect : '/profile',
            failureRedirect : '/'
        }));

    //unlink
    app.get('/unlink/google', function(req, res) {

        var user          = req.user;

        //check if has no more links to account
        if (typeof user.facebook.token == "undefined" && !user.local.verified) return res.render(PATH_ERROR, { errorMessage: "Cannot unlink all accounts."});

        user.google.token = undefined;
        user.save(function(err) {
            res.redirect('/profile');
        });
    });

}