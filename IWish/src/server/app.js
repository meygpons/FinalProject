const express 	   = require("express");
const bodyParser   = require("body-parser");
const mongoose 	   = require("mongoose");
const cookieParser = require('cookie-parser');
const session 	   = require('express-session');
const passport     = require('passport');
const fileUpload   = require("express-fileupload");
const path 	       = require('path');
const app 		   = express();
const router       = express.Router();
const appi         = require("./appi.js");

router.use(express.static('../resources')); // @FIXME

router.use(fileUpload());
router.use(cookieParser());
router.use(bodyParser.urlencoded({ extended: false }));
router.use(bodyParser.json());

router.use(session({ secret: 'molonmolonagatitobonitoysuavecito' })); // session secret
router.use(passport.initialize());  
router.use(passport.session());

// por defecto los templates se buscan en views del directorio de app
// pero en este caso no queremos que sea asi, por lo tanto lo cambiamos
app.set('views', __dirname);
app.set("view engine", "pug");

//https://mlab.com/login/
//Username: iwishA
//pw: ******** ver libreta secreta
mongoose.connect("mongodb://iwishA:1W15h!B3niS*@ds137441.mlab.com:37441/iwish", function(err, res) {
	if (err) console.log("error al conectarse a la bd");
	else console.log("conectat a la bd");
});
/*mongoose.connect("mongodb://localhost/iwish", function(err, res) {
	if (err) console.log("error al conectarse a la bd");
	else console.log("conectat a la bd");
});/*

//*** confirm mail ***/
//require("../components/login/local/controller/login.local.confirm.controller.js");

//*** logins ***//
require("../components/login/commons/controller/login.commons.controller.js")(passport);
require("../components/login/commons/routes/login.commons.router.js")(router);

require("../components/login/facebook/controller/login.facebook.controller.js")(passport);
require("../components/login/facebook/routes/login.facebook.router.js")(router, passport);

require("../components/login/google/controller/login.google.controller.js")(passport);
require("../components/login/google/routes/login.google.router.js")(router, passport);

require("../components/login/local/controller/login.local.controller.js")(passport);
require("../components/login/local/routes/login.local.router.js")(router, passport);
//*** fin logins ***//

//*** profile ***//
// @FIXME
require("../components/profile/routes/profile.router.js")(router);

//*** settings ***//
require("../components/settings/routes/settings.router.js")(router);

//*** post ***//
require("../components/post/routes/post.router.js")(router);

//*** footer pages ***//
require("../components/footer_pages/controller/footer.controller.js");
require("../components/footer_pages/routes/footer.router.js")(router);

//*** JWT generation cheat ***//
require("../components/api_commons/routes/api_commons.router.js")(router);

/*require("../routes/router.controller.js")(router, passport)*/;
router.use(function(req, res){
	res.render("../views/error/404.pug");
});

//*** routers ***//
app.use("/api/", appi);
app.use("/", router);

app.listen(8008, function() {
	console.log("node server running at localhost:8008")
});
