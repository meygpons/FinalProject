#!/bin/sh

su -c "killall nginx; nohup nginx -p . -c ./config/nginx/nginx.conf > /dev/null &"
pushd src/server
forever stop app.js
forever start app.js
forever logs app.js -f
