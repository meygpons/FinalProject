HOTSEX será un motor de templating -generación dinámica de páginas web- que permitirá a los desarrolladores web generar páginas web dinámicas, utilizando métodos muy flexibles y más rápidos de escribir que HTML, escritas únicamente en expresiones S (la "sintaxis de árbol" con paréntesis que usa Lisp) y cabeceras que definan los datos que requieren de la base de datos.

### ANÁLISIS FUNCIONAL
HOTSEX debe ser capaz de: 
* Atender peticiones HTTP enviadas por un proxy inverso como nginx.
* Generar código HTML en función de archivos fuente escritos en nuestro propio DSL -Domain Specific Language, o lenguaje de dominio específico-.
* Ser rápido y no bloqueante, ergo concurrente.
* Contactar la base de datos para recuperar datos y "expandirlos" durante el procesado del template
* OPCIONAL: si hay tiempo, mejorar los tiempos de carga aún más utilizando un sistema de cacheado inteligente, que invalide los cachés generados cuando los datos relacionados en la base de datos sean actualizados.
* OPCIONAL: si sobra mucho tiempo, desarrollar un lenguaje de scripting básico Turing completo para usar dentro de nuestro DSL.

### ANÁLISIS FUNCIONAL
HOTSEX está en fases tempranas de desarrollo, pero tenemos alguna idea de cómo queremos hacerlo:
* Utilizaremos Rust para programar HOTSEX, ya que es un lenguaje rápido, seguro y que permite concurrencia sin complicaciones.
* Tendremos un hilo que escuche peticiones, otro que actúe como "broker" de trabajos con un stack FIFO de tareas, y n hilos que atiendan los trabajos de los anteriores.
  * EDIT: para la version preliminar, tendremos un productor de sockets, que envía sockets a un procesador de peticiones, que envía trabajos al hilo de parsing y composición de páginas, que envía el resultado a un hilo que devuelve los datos al cliente
  * Podríamos utilizar una librería de actores para manejar los hilos, o hacerlo a mano ya que no es extremadamente difícil.
* HOTSEX debería tener la capacidad de atender peticiones HTTP, pero no sería necesario que atendiese peticiones HTTPS o tener todas las capacidades que tiene un servidor web normal, ya que estará siempre puesto detrás de un servidor web o proxy inverso como nginx. 
  * Podemos utilizar una librería de servidor web para Rust, o programar nosotros el nuestro, que no es excesivamente difícil.
* Necesitaremos un parser para nuestro DSL. Por suerte, las expresiones S son muy fáciles de interpretar, y posiblemente es una de las sintaxis mejor documentadas y con más facilidades que existe.
  * Se podría utilizar una librería o generador de parsers para ello.
