#![deny(unsafe_code)]

use hotsex_structs::{HotsexAtom, HotsexNode, HotsexTree};
use casting::TryFrom;
use parser;
use self::parser::{Atom, Sexp};
use resource_loader;

use std::fs::File;
use std::io::BufReader;
use std::convert::From;
use std::collections::HashMap;
use std::str::Split;
use std::fmt;

pub enum Error {
    ParseError(parser::Error)
}

pub const META_TAG:       &'static str = "!";
pub const ATTRIBUTES_TAG: &'static str = "@";
pub const ID_TAG:         &'static str = "#";
pub const CLASS_TAG:      &'static str = ".";
pub const VECTOR_TAG:     &'static str = "'";

pub const CLASS_ATTRIBUTE: &'static str = "class";
pub const ID_ATTRIBUTE:    &'static str = "id";

// Similar to the parser's quote function, but without the unnecessary Cow
/// Puts a string between quotes, escaping all necessary characters inside
fn quote(s: &str) -> String {
    // Generally speaking, we will only have two extra quotes
    let mut r: String = String::with_capacity(s.len() + 2);
    
    r.push_str(&s.replace("\\", "\\\\").replace("\"", "\\\""));
    r.push_str("\"");

    r
}

/// Gets the contents of an atom as a String, but without the quotes that would
/// appear otherwise in Text atoms if we were to use to_string
fn get_raw_atom_content_as_string(a: &HotsexAtom) -> String {
    match *a {
        HotsexAtom::Text(t) => t,
        any                 => any.to_string()
    }
}

impl fmt::Display for HotsexAtom {

    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        let s = match *self {
            HotsexAtom::Symbol(s) => s,
            HotsexAtom::Text(t) => quote(&t),
            HotsexAtom::Integer(i) => i.to_string(),
            HotsexAtom::Float(f) => f.to_string()
        };

        write!(f, "{}", s)
    }

}

impl From<Atom> for HotsexAtom {

    fn from(a: Atom) -> Self {
        match a {
            Atom::S(s) => HotsexAtom::Symbol(s),
            Atom::T(t) => HotsexAtom::Text(t),
            Atom::I(i) => HotsexAtom::Integer(i),
            Atom::F(f) => HotsexAtom::Float(f)
        }
    }

}

/// Searches for coincidences on the first item of each node branch. Does not
/// search recursively. Returns a Some if found, or a None if not found.
/// This function could work as some linear "hashmap" search.
/// Please note this function IGNORES Leaf nodes, as they are dangling keys on
/// a different level and thus can't be considered entries.
fn search_node_branches<'a>(k: HotsexAtom, v: &'a mut Vec<HotsexNode>) 
                            -> Option<&'a mut HotsexNode> {
    for node in *v {
        if let HotsexNode::Branch { children, .. } = node {
            if let Some(&HotsexNode::Leaf { value, .. }) = 
                children.first() {
                if value == k { 
                    return Some(&mut node); 
                }
            }
        }
    }
    None
}

/// Searches for the first coincidence of a HotsexNode::Leaf in a 
/// Vec<HotsexNode>. Returns a Some with a mutable reference to the found node,
/// or None if not found.
fn search_node_leaves<'a>(k: HotsexAtom, v: &'a mut Vec<HotsexNode>) 
                            -> Option<&'a mut HotsexNode> {
    for node in *v {
        if let HotsexNode::Leaf { value, .. } = node {
            if value == k { 
                return Some(&mut node); 
            }
        }
    }
    None
}

/// Searches for the first coincidence of a HotsexNode in a 
/// Vec<HotsexNode>. Returns a Some with a mutable reference to the found node,
/// or None if not found.
fn search_node<'a>(k: HotsexAtom, v: &'a mut Vec<HotsexNode>) 
                   -> Option<&'a mut HotsexNode> {
    for node in *v {
        match node {
            HotsexNode::Leaf { value, .. } =>  {
                if value == k { 
                    return Some(&mut node); 
                }
            },
            HotsexNode::Branch { children, .. } => {
                if let Some(&HotsexNode::Leaf { value, .. }) = 
                    children.first() {
                    if value == k { 
                        return Some(&mut node); 
                    }
                }
            }
        }
    }
    None
}

/// Attempts to get a HotsexNode from a given Vec<HotsexNode> and
/// return a mutable reference to it. The function inserts a new branch if it
/// did not already exist.
fn get_node_from_key<'a>(k: HotsexAtom, 
                         attrs: &'a mut Vec<HotsexNode>)
                         -> &'a mut HotsexNode {
    // @TODO: Flatmap attributes inside attributes
    let attr: &'a mut HotsexNode = 
        if let Some(node) = search_node_branches(k, attrs) {
            node
        } else {
            attrs.push(HotsexNode::Branch {
                children:   vec![HotsexNode::Leaf {
                                    value: k,
                                    tag: None
                                 }],
                tag:        None,
                meta:       None,
                attributes: None
            });
            // Should be safe. We just added it
            attrs.last_mut().unwrap()
        };
    
    attr
}

/// Given a class-tagged atom (or not, but it's intended for class atoms), it
/// try to decompose it in CLASS_TAG-separated pieces, and then will proceed
/// to push each one of them to the (class ..) node inside the given attrs Vec.
/// This function will generate a (class ..) node if it doesn't exist already.
fn merge_atom(atom: &HotsexAtom,
              attrs: &mut Vec<HotsexNode>,
              attribute_name: String,
              split_by: &str) {
    // we can't split the atom itself since that would cause a nuclear reaction
    let content =  get_raw_atom_content_as_string(atom);

    let split = content.split(split_by);
    let old_members: &mut Vec<HotsexNode> = 
        if let &mut HotsexNode::Branch { ref mut children, .. } =
               get_node_from_key(HotsexAtom::Symbol(attribute_name), attrs) {
            children
        } else {
            unreachable!();
        };

    for item in split {
        old_members.push(
            HotsexNode::Leaf {
                // maybe Symbol?
                value: HotsexAtom::Text(item.to_string()),
                tag: None
            }
        );
    }
}

/// Given a vector of HotsexNode to pull from and a vector of HotsexNodes to 
/// push to, attempts to merge their children by using the each branch's first 
/// symbol or the atom as the foreign key.
///
/// Branch-to-branch merges are performed normally, by keeping the first symbol
/// and joining both of them non-recursively.
/// Atom-to-branch merges will discard the atom and do nothing
/// Branch-to-atom merges will push the whole branch
/// Atom-to-atom merges will do nothing
/// Anything-to-nonexistant will simply push it into the other vector
///
/// INPUT TAGS ARE DROPPED. META AND ATTRIBUTES ARE MERGED AS WELL
fn merge_sexp_as_map(from: Vec<HotsexNode>, into: &mut Vec<HotsexNode>) {
    'outer: for node in from.into_iter() {
        match node { 
            HotsexNode::Branch { children, meta, attributes, .. } => {
                // Found a branch. Let's peek into its first item
                match children.first_mut() {
                    Some(&mut HotsexNode::Leaf{ value, tag }) => {
                        // Found a leaf (as expected). Let's try to search for
                        // it in the into vector
                        let mut node_found = search_node(value, into);
                        if let Some(ref mut existing_node) = node_found {
                            // Found it
                            match **existing_node {
                                HotsexNode::Leaf { .. } => {
                                    // It's a leaf. Replace it by the from node
                                    *existing_node = &mut node;
                                },
                                HotsexNode::Branch { 
                                    children: ref mut into_children, 
                                    meta: ref mut into_meta,
                                    attributes: ref mut into_attrs,
                                    ..
                                } => {
                                    // It's a branch. Push all but the first
                                    // item in the from node into the existing
                                    // node
                                    for item in children.into_iter().skip(1) {
                                        into_children.push(item);
                                    }
                                    
                                    // Let's check the status of both source
                                    // and target meta
                                    match (meta, into_meta) {
                                        (Some(from_meta), 
                                         &mut Some(target_meta)) => {
                                            // Both exist. Merge them
                                            for meta_item in 
                                                from_meta.into_iter() {
                                                // No need for complicated 
                                                // logic, we just merge them 
                                                // raw
                                                target_meta.push(meta_item);
                                            }
                                        },
                                        (Some(from_meta), &mut None) => {
                                            // Only source exists. Set target
                                            // to source
                                            *into_meta = Some(from_meta);
                                        },
                                        _ => () // Source is None. Do nothing
                                    }
                                    
                                    // Let's check the status of both source
                                    // and target attributes
                                    match (attributes, into_attrs) {
                                        (Some(ref from_attr),
                                         &mut Some(ref mut target_attr)) => {
                                            // Both exist. Merge them
                                            for attr_item in 
                                                from_attr.into_iter() {
                                                // Perhaps we should flatmap 
                                                // them
                                                //merge_sexp_as_map(from_attr,
                                                //                  target_attr);
                                            }
                                        },
                                        (Some(from_attr),
                                         &mut None) => {
                                            // Only source exists. Set target
                                            // to source
                                            *into_attrs = Some(from_attr);
                                        },
                                        _ => () // Source is None. Do nothing
                                    }
                                }
                            }
                        } else {
                            // node was not found. Just push all of it
                            into.push(node);
                        }
                    },
                    // We can't really merge something with a branch as its
                    // key, so we just push it.
                    Some(&mut HotsexNode::Branch { .. }) => into.push(node),
                    _ => () /* do nothing */
                }
            },
            HotsexNode::Leaf { value, .. } => {
                // Found a leaf. Let's check if doesn't exist in the target
                if let None = search_node(value, into) {
                    // Doesn't exist. Let's push it
                    into.push(node);
                }
                // Otherwise, the key exists, so it's pointless to push it
                // there
            }
        }
    }
}

impl From<Sexp> for HotsexNode {
    
    fn from(s: Sexp) -> Self {
        match s {
            Sexp::Atom(a, t) => {
                HotsexNode::Leaf{ value: HotsexAtom::from(a), 
                                  tag:   t }
            },
            Sexp::List(v, t) => {
                // This part is kinda tricky. We have to recursively traverse
                // the whole vector, and then we must identify where each sexp
                // must go based on their tag.
                //
                // Possible meaningful tags:
                //  - '!':  meta tag. These sexps are added to the meta field of
                //          our branch item. The sexp must not be pushed into
                //          the children list. The tag is kept. IF FOUND IN AN
                //          ATOM, THIS TAG MUST BE IGNORED!
                //  - '@':  attributes tag. These sexps are added to the
                //          attributes list of our branch item. The sexp must
                //          not be pushed into the children list. The tag is
                //          kept.
                //  - '#':  id tag. These will go on the attributes field of our
                //          branch item. These items should not be pushed into 
                //          the children list. The tag is not kept.
                //  - '.':  class tag. These atoms will go on the class member
                //          of our branch's attributes list. These atoms may pack
                //          more "classes" inside each one of them, separated by
                //          dots (a split should suffice). They all must be put
                //          under the class attribute of the list. The sexp must
                //          not be pushed into the children list. The tag is not
                //          kept.
                //  - None: untagged or otherwise not contemplated tags must go
                //          to the children field of our branch object. Tags
                //          are kept, if any.

                // We transform every node in the children to HotsexNodes, thus
                // recursively forming the tree
                let transformed_nodes: Vec<HotsexNode> = 
                    v.into_iter().map(HotsexNode::from).collect();
                
                // @TODO: add some default capacity to vectors to avoid
                //        excessive reallocation.
                let mut child: Vec<HotsexNode> = Vec::new();
                let mut meta: Vec<HotsexNode>  = Vec::new();
                let mut attrs: Vec<HotsexNode> = Vec::new();

                // Enjoy the ride
                for node in transformed_nodes {
                    match node {
                        HotsexNode::Leaf { value: val, tag: tag } => {
                            // Leaf found. Let's check the tag
                            match tag {
                                Some(tagString) => {
                                    // Tag found.
                                    match &*tagString {
                                        CLASS_TAG => 
                                            // It's a class leaf! Merge all
                                            // the CLASS_TAG-separated classes
                                            // into attrs' CLASS_ATTRIBUTE
                                            // entry
                                            merge_atom(&val, &mut attrs,
                                                       CLASS_ATTRIBUTE
                                                            .to_string(), 
                                                       CLASS_TAG),
                                        ID_TAG => 
                                            // Why does this split an id
                                            // atom by ID_TAG? The answer
                                            // is "yes".
                                            merge_atom(&val, &mut attrs,
                                                       ID_ATTRIBUTE
                                                            .to_string(),
                                                       ID_TAG),
                                        // It's any other tag. Just push it.
                                        _   => child.push(node)
                                    } 
                                },
                                // No tag found. Just push it
                                None => child.push(node)
                            }
                        },
                        HotsexNode::Branch { tag, children, .. } => {
                            // Branch found. Let's check for tags
                            match tag {
                                Some(tagString) => {
                                    // Tag found.
                                    match &*tagString {
                                        META_TAG =>
                                            // It's a meta tag. These don't
                                            // work as maps, so just jam it in.
                                            meta.push(node),
                                        ATTRIBUTES_TAG =>
                                            // Attributes tag. Merge it
                                            // map-style into attributes
                                            merge_sexp_as_map(children, 
                                                              &mut child),
                                        ID_TAG =>
                                            // Id tag. This must be merged
                                            // map-style into attributes under
                                            // the (id ..) entry
                                            //merge_sexp_as_map(children, )
                                            (),
                                        CLASS_TAG =>
                                            // Class tag. This must be merged
                                            // map-style into attributes under
                                            // the (class ..) entry
                                            //process_attr_sexp()
                                            (),
                                        _   => child.push(node)
                                    }
                                }, None => child.push(node)
                            }
                        }
                    }
                }

                HotsexNode::Branch {
                    children: child,
                    tag: t,
                    meta: if meta.len() > 0 { Some(meta) } else { None },
                    attributes: if attrs.len() > 0 { Some(attrs) } else { None }
                }

            }
        }
    }
}

/*
impl From<parser::Sexp> for HotsexTree {

    fn from(s: Sexp) -> Self {
        // For every children, meta and attributes in the resulting HotsexNode,
        // recursively check for meta tags and possible (include ..) entries
    }

}

impl TryFrom<File> for HotsexTree {
    type Err = Error;
    fn try_from(f:  File) -> Result<Self, Self::Err> {


    }
}
*/
