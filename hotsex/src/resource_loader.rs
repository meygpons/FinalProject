//! A simple resource loader capable of loading and processing HOTSEX files.

#![deny(unsafe_code)]

use parser;
use hotsex_structs::{HotsexAtom, HotsexNode, HotsexTree};
use casting::TryFrom;

use std::collections::HashMap;
use std::error;
use std::fmt;
use std::cmp;
use std::hash;
use std::fs::File;
use std::borrow::Cow;
use std::cell::RefCell;

#[derive(Debug)]
pub enum Error {
    ResourceMissError(&'static str),
    BadResourceError(&'static str)
}

impl error::Error for Error {

    fn description(&self) -> &str {
        // Why wouldn't they implement specific types for enum members
        // @HACK: why am I even doing this to myself
        match self {
            &Error::ResourceMissError(m) => m,
            &Error::BadResourceError(b) => b
        } 
    }

    fn cause(&self) -> Option<&error::Error> { None }
}

impl fmt::Display for Error {

    fn fmt(&self, f: &mut fmt::Formatter) -> Result<(), fmt::Error> {
        write!(f, "{}", error::Error::description(self))
    }

}

/// A ResourceLayer is an entity capable of returning a T entity, with total
/// abstraction from the underlying methods, as well as being able to accept
/// new resources.
pub trait ResourceLayer<K: cmp::Eq + hash::Hash, V: Clone> {

    /// Given a K key, returns an optional V value
    fn acquire_resource(&self, &K) -> Result<Cow<V>, Error>;
    /// Returns whether this layer will accept a given resource in put_resource
    /// or not
    fn would_put(&self, &K, &V) -> bool;
    /// Attempts to store a given K key and V value. Returns true if
    /// successful, false otherwise
    fn put_resource(&mut self, K, V) -> bool;
}

/// A simple, HashMap-based cache layer for heap storage of values
#[derive(Default)]
pub struct CacheLayer<K: cmp::Eq + hash::Hash, V: Clone> {

    map: HashMap<K, V>

}

impl <K: cmp::Eq + hash::Hash, V: Clone> ResourceLayer<K, V> for CacheLayer<K, V> {

    fn acquire_resource(&self, key: &K) -> Result<Cow<V>, Error> {
        match self.map.get(key) {
            Some(value) => Ok(Cow::Borrowed(value)),
            None => Err(Error::ResourceMissError("Could not find resource in hashmap."))
        }
    }

    fn would_put(&self, key: &K, value: &V) -> bool {
        // This layer stores pretty much everything you throw at it
        true
    }

    fn put_resource(&mut self, key: K, value: V) -> bool {
        self.map.insert(key, value);
        true
    }

}

/// Tries to find a given resource in the underlying filesystem
#[derive(Default)]
pub struct FilesystemLayer {

    pub path_prepend: String
    
}

impl <'a, V: TryFrom<File> + Clone> ResourceLayer<String, V> for FilesystemLayer {

    fn acquire_resource(&self, key: &String) -> Result<Cow<V>, Error> {
        let full_path = format!("{}{}", self.path_prepend, key);
        let file = File::open(full_path);
        match file {
            Ok(f) => match V::try_from(f) {
                Ok(res) => Ok(Cow::Owned(res)),
                Err(_) => Err(Error::BadResourceError("Could not parse resource"))
            },
            Err(_) => Err(Error::ResourceMissError("Could not open file"))
        }
    }

    fn would_put(&self, key: &String, value: &V) -> bool {
        // This layer stores nothing
        false
    }

    fn put_resource(&mut self, key: String, value: V) -> bool {
        false
    }

}

/// Stores several ResourceLayers in a vector, then calls them one by one in
/// order of insertion. If any of them returns a hit, the DaisyLayer returns
#[derive(Default)]
pub struct DaisyLayer<K: cmp::Eq + hash::Hash, V: Clone> {
    // Hi! I'm Daisy!

    resource_layers: Vec<Box<ResourceLayer<K, V>>>
    
}

impl <K: cmp::Eq + hash::Hash, V: Clone> DaisyLayer<K, V> {
    
    fn push_layer(&mut self, layer: Box<ResourceLayer<K, V>>) {
        self.resource_layers.push(layer)
    }

    fn pop_layer(&mut self) -> Option<Box<ResourceLayer<K, V>>> {
        self.resource_layers.pop()
    }

}

impl <K: cmp::Eq + hash::Hash, V: Clone> ResourceLayer<K, V> for DaisyLayer<K, V> {

    fn acquire_resource(&self, key: &K) -> Result<Cow<V>, Error> {
        for layer in &self.resource_layers {
            if let Ok(resource) = layer.acquire_resource(key) {
                // Resource found
                return Ok(resource)
            }
        }
        // If it was not found, return an error
        Err(Error::ResourceMissError("Could not find resource in any layer"))
    }

    fn would_put(&self, key: &K, value: &V) -> bool {
        for layer in &self.resource_layers {
            if layer.would_put(key, value) {
                return true
            }
        }
        false
    }

    fn put_resource(&mut self, key: K, value: V) -> bool {
        for layer in &mut self.resource_layers {
            if layer.would_put(&key, &value) {
                return layer.put_resource(key, value);
            }
        }
        false
    }

}
// @TODO: load-balanced cache layer
